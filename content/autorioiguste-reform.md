+++
title="Paremad autoriõigused tänapäevaks"
date=2021-10-07
+++

Autoriõigused on seadused mis annavad autoritele õiguse oma loomingut levitada
ja müüa. Tänapäeval kehtivad autoriõigused ei ole sobilikud interneti jaoks,
kuna igasukuste materjalide levitamine on nii lihtne.

<!-- more -->

See postitus põhineb Rick Falkvinge ja Christian Engströmi raamatu
<q>[The Case for Copyright Reform](http://falkvinge.net/wp-content/uploads/large/The%20Case%20For%20Copyright%20Reform%20(2012)%20Engstrom-Falkvinge.pdf)</q>
teisel peatükil.

Mina nõustun raamatus välja toodud punktiga et reformi on vaja.
Inimestel peaks olema vabadus privaatsusele ja autoriõiguste pidev marss 
kontrolli üle on hakkanud seda vabadust piirama.
Raamatus on välja toodud mitu lahendust.

# Vaba levitamine mitte tulu teenimise eesmärgil
Üks lahendus mis raamatus välja tuuakse on anda inimestele vabadus levitada
autoriõigustega kaitstud töid niikaua kui nad sellest tulu ei teeni. See
lahendus loomulikult tähendab, et inimesed igasuguseid asju lihtsalt
internetist alla laadida nende eest maksime eest, aga seda tehakse tehakse
ka tänapäeval.

# Autoriõiguste piiramine 20 aastani
Praeguse seaduse järgi on autoril ekskluusivne õigus kuni 70 aastat peale tema
surma. Raamatus on soovitatud lubada autoril ainuõigust ainult 20 aastat. Mina
arvan, et see on hea idee. 20 aastat on piisavalt kaua, et autor saab oma
loomingu eest kasu teenida ja 20 aasta tagant saavad ka teised seda
inspiratsiooniks kasutada.

# DRMi keelustamine
Veel üks soovitus raamatust on keelata ära erinevatel kompaniidel oma loomingule
ligipääsu keelata. See on loomulikult vajalik, kuna kompaniid saavad läbi selle
keelata inimestel vabalt asju levitada isegi kui nendel on selleks õigus.
