+++
title="Mis on proff?"
date=2021-10-23
+++
kui rääkida terminist proff, siis võib õelda, et seda kasutatakse peamiselt
koguemuse näitamiseks, aga sellel sõnal ei ole kindlat definitsiooni. Enamus
inimesi kasutab seda tunde põhjal. Seetõttu ei ole mingit kindlat viisi aru
saada mis on proff, kuna igal inimesel võib olla oma arusaam mis asi proff on.

<!-- more -->

Näiteks vöib öelda, et proff on inimene kes teeb midagi professionaalselt, ehk
teeb midagi oma tööna ja talle makstakse selle eest. Selleks, et sulle millegi
eest makstakse tähendab, et sa oskad midagi hästi. Kuid seellise arusaamaga on
sõna proff nõrgem enamuse arusaamust, kuna see tähendaks, et iga inimene kes
mingil alal töötab on selle ala proff.

Osad võivad arvata, et proff on üks oma ala parimaid. Seda on aga väga
keeruline mõõta. Kui palju peab inimene oma alast teadma et teda proffiks
kutsutaks? Kas ta peab olema kirjutama teadusliku paberi? või läbi pääsema
mingisugusest testist?

Lisaks on ka sertifikatsioonid, mis sammuti näitavad ala oskust. Võib ju õelda,
et igaüks kes on saanud alal sertifikatsiooni on selle ala oskaja. Aga ka see
ei väljenda koguemust.
