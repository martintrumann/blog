+++
title="Interneti reeglid"
date=2021-10-01
+++

Minu kokkupuude enamus sotsiaalmeediaga on väike, aga siiski olen ka mina
näinud väga hästi kuidas inimesed internetis käituvad.

<!-- more -->

Mina kasutan peamiselt ainult kolme keskonda: Youtube, Twitch ja Reddit.
Ma kasutan neid peamiselt selle pärast, et need vajavad minu poolt minimaalset
pingutust asjade leidmiseks. Youtube's ja Twitchis on ainult osas kanalid mida
ma vaatan ja enamus saiti mind eriti ei huvita. Seetõttu on reddit see koht kus
ma peamiselt teisi internetikasutajaid näen.

Internetis on väga lihtne kaotada oma inimlikus. Tänu anonüümsele suhtlusele ja
üldisele internetikultiirile on solvangud ja ähvardused lihtsasti tulema.
Internetis valitseb vägagi emotsiooniline suhtlus, kuna inimesed räägivad palju
ja eriti ei mõtle enne kui midagi ütlevad. Anonüümsus võib anda inimestele
tunde et naad on puutumatud ja võivad rääkida ükskõik mida ja ükskõik kuidas.

Internetus on aga tulnud ette kohti kus olen näinud kahte inimest väitlemas.
Üks postitab mingi argumendi ja teine vastab sellele võttes iga argumendi
punkti ja selle üle arutledes. Kahjuks juhtub seda väga harva.
