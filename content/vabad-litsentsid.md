+++
title="Vabad litsentid ja nende mõju"
date=2021-10-16
+++

Vabasid litsentse on erinevaid ja osad nendest ei luba tarkvara kasutada ilma
sama litsentsi kasutamata.

<!-- more -->

# GPL
GPL on tugeva edasikanduvisega litsents. Iga tarkvara mis on GPL litsentsiga
ei tohi kopeerida ilma GPL kasutamata. Üks kuulsamaid tarkvarasid selle
litsentsiga on linuxi kernel.

# LGPL
LGPL on natuke nõrgem litsents kui GPL. LGPLiga tarkvara võib kasutada ilma, et
suurem programm peab mingeid muutuseid tegema, aga muutatused tarkvarale peavad
LGPLiga olema. Seda litsentsi kasutavad peamiselt erinevad teegid.

# MIT
MIT litsents ei ole "copyleft" litsents. See tähendab, et selle litsentsiga
tarkvara võib muuta kuidas tahad ja muudetud tarkvara ei pea sama litsentsiga
levitama. Selle litsentsi kasutavad enamus uued vaba tarkvara projektid kuna
see on lihtne ja arusaadav.

