+++
title="Uus meedia mõju traditsioonilisele meediale"
date=2021-09-17
+++
Uut meediat saab isikupärastada selle lihtsuse poolest. tänapäeval on Igaühel
võimalus uut meediat teha ja jagada. Erinevalt traditsioonilisest meediast kus
läks levitamiseks vaja rohkem kui ainult internetiühendust. See aitab
traditsioonilisel meedial välja tuua oma kvaliteeti ja profesionaalsust.

<!-- more -->

Uue meediat saab luua igaüks kellel on natukenegi tahtmist. Tänu sellele on 
seda maailmas väga palju, kuigi aega selle tegemiseks on olnud suhteliselt vähe
traditsioonilise meediaga vörreldes.

Traditsiooniline meedia ei ole aga ka kuhugi kadunud. Selle asemel on see ka
üle liikunud internetti kust on seda paljudel lihtsam jälgida. Enamustel
traditsioonilistel meediakanalitel on mingisugune internetivorm. Telesaated on
järelvaatatavad, artikkleid ei pea enam paberi pealt lugema jne. See aga ei
tähenda, et traditsiooniline meedia on muutunud vähem kvaliteetesemaks.

Üks mõju mis uue meedia tulekuga kaasneb võib olla traditsioonilise meedia
vaatamise langus. Tänapäeval on inimestel palju lihtsam tarbida uut meediat mis
toob endaga kaasa traditsioonilise meedia jälgimise vähenemise.
Uue meedia tulekuga on traditsioonilisel meedial vähem publikut kuna inimesed
kasutavad uut meediat.

Traditsiooniline meedia on aga kvaliteetsem kui uus meedia.
Kuna uus meedia oma olemuselt vaba, siis toob see välja traditsioonilise meedia
kvaliteetsuse palju rohkem. See võib aga osa inimesi traditsioonilist meediat
vaatama panna.
