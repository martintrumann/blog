+++
title="IT arengu lootused ja reaalsus"
date=2021-09-24
+++

Eesti infoühiskonna arengukava 2020 tõi välja mitmed visioonipunktid. Osad
neist realiseerustid rohkem, osad vähem. Siin on minu arvates kõige rohkem
ja vähem realiseerunud punktid.

<!-- more -->

# Kõige rohkem realiseerunud
> __Töökohad on kõigis asutustes ja ettevõtetes võrgustunud__, mis on muutnud
> töö tegemise paindlikuks nii Eestis kui ka rahvusvaheliselt tegutsedes. See
> võimaldab muu hulgas rohkem kaugtööd ja osaajaga tööd – nii on saanud
> tööturule tulla ka töötegijad, kes on seni pidanud jääma eemale. Ettevõtete
> jaoks tähendab see võimalust tööjõudu jagada ja inimressurssi paindlikumalt
> juhtida.

See punkt on realiseerunud ainult tänu viimase kahel aastal toimunud
viirusepuhangule. Ilma vajaduseta oma töötajatel kodus töötada, ei usu ma, et
enamus tööandjaid oleks sellsist töövormi kasutanud, aga nüüd kui nad on seda
viimased aastad kasutatud siis jääb see võimalus ilmselt ka püsima kui
alternatiiv kontoris töötamisele.

> Eestisse __tullakse uudseid IKT-lahendusi tegema ja katsetama kogu
> maailmast__. Siin loovad uusi tervishoiu, tööstuse, energeetika, hariduse jt
> lahendusi uued rahvusvahelised arenduskeskused, välismaised kasvuettevõtted
> ja teadurid.

Erinevalt eelmisele punktile on see punkt jõustunud naturaalselt tänu eesti
attraktiivsusele uute firmade suhtes. Eestis on kasvanud palju selliseid
firmasid mis kasutavad IT-lahendusi täielikult et kastsetada ja leida uusi
viise asju teha.

# Kõige vähem realiseerunud
> __Haridus__ on muudetud IKT võimaluste mitmekesise kasutamise najal
> __personaliseerituks ja paindlikuks, sealhulgas ümber- ja täiendõpe__. Käib
> agar teadmiste ja oskuste omandamine kiirelt ja pidevalt kogu elu jooksul.

Haridus on tänapaeval arenenud vägagi vähe. Loomulikult on tänapäaeval koolid
IT lahendusi kasutama hakkanud, aga kogu õppimise protsess on ikkagi üldjoones
samasugune kui kümme aastat tagasi. Õppimine on läinud tänu järelvaatamise
võimalusele natuke paindlikumaks, aga IT lai levimine ei ole muutunud haridust
personaliseeritumaks, vaid on ainult täjendanud olemasolevat õppeprogrammi.
