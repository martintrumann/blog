+++
title="Veebi kasutatavus"
date=2021-11-21
+++

Minu arvates on enamus veebilehti on hea kasutatavusega.
Nad on lihtsasti navigeeritavad ja neilt on lihtne kiiresti infot kätte saada.
Kui mingi leht nii lihtne ei ole siis see tuleneb enamasti tundmatusest, mitte
lehe halvast kasutatavusest.

<!-- more -->

Üks näide hea kasutatavusega lehest on näiteks [ArchWiki](wiki.archlinux.org).
Sellel lehel on lihtne disain ja väga hea otsing mis leiab õige lehe kiiresti
üles ja lehed näitavad kogu vajaliku informatsiooni selgesti ära.

Negatiivset näited kasutatavusest ma eriti tuua ei oska, kuna ma ei mäleta
ühtegi lehte mis on halva kasutatavusega olnud. Enamasti on probleemid tekkinud
lehe struktuurist mitte aru saamisest, aga pärast lehe kasutamist sellised
probleemid kaovad. Seetõttu ei oska ma mitte ühtegi näidet siia tuua.
