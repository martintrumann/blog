+++
title="Kolm huvitavat asja"
date=2021-09-03
+++
Mina valisin oma kolmeks it-lahenduseks asjad millega olen ise väga tutav.
Kõiki kolme olen ma isiklikult kasutanud ja tahan tuua natuke rohkem tähelepanu
nendele.

<!-- more -->

# Arch linux (2000 - 2010)
[Arch linux](https://archlinux.org) on minimalistlik linuxi distro. Mina olen seda kasutanud 3 aastat.
Minule Arch vägagi meeldib, kuna see lubab mul kasutada kõige uuemat tarkvara
Archi installimine on natuke erinev teistest distrotest kuna sellel ei ole
tavalist graafilist installerit. Seetõttu peavad selle kasutajad linuxi
tööprotsesse teadma enne kui nad seda kasutavad. aga selle installimine annab
ka väga vajalikud kogemused linuxiga töötamise jaoks.

Archlinux sai alguse 2002 aastal ja on tänaseni kasutatud operatsioonisüsteem.

Algsel installil ei ole archil midagi installitud. Arvuti lahti tegemisel
vaatab sulle otsa tume ekraan ja ligi pääsed ainult läbi CLI. Pärast osade
pakettide installimist aga on täiesti vöimalik tavaliselt archis tööd teha.

Archi pakettihaldur on pacman. Seda on vägagi lihtne kasutada. Arvuti
uuendamiseks pead lihtsalt jooksutama `sudo pacman -Syu` ja mingi programmi
installimiseks `sudo pacman -S <nimi>`.


# Matrix (2010 - 2020)
[Matrix](https://matrix.org) on detsentraliseeritud internetisõnumi protokoll.
See erineb teistest sarnastest keskondadest tänu oma vabadusele matrixi
kasutaja ei ole lukkustatud ainult ühte programmi, selle asemel vöib ta suhelda
ükskõik millise matrixi programmiga. Sama põhjuse pärast ei pea kasutaja tegema
mitu erinevat kasutajat erinenate programmide jaoks vaid vöib ainult oma
matrixi kasutajaga ükskõik millisesse programmi siseneda.

Matrix loodi aastal 2014 ja sai 1.0 versiooni 2019 aasta juunis. Seda
kasutatakse peamiselt vabavara projektide poolt, aga on viimastel aastatel
euroopa riikide avaliku sektori poolt kasutusele võetud ning ka osad euroopa
ülikoolid on seda kasutama hakkanud.

Matrix saab ka kõikide teise _chat_-itdega suhelda läbi nn "sildade". Sillad
lubavad ühendada oma teised portaalid (Slack, Discord, IRC, Messenger) matrixiga
ja suhelda ka nendega läbi ühe programmi. Lisaks sellele saab tänu botidele
täjendada niikaua kui kujutlusvöime lubab. Mina kasutan seda näiteks RSS
lugejana.

Mina usun, et matrix on veebikommunikatsiooni tulevik kuna see annab kasutajale
võimu kasutada seda programmi mida ta ise tahab. Mina olen matrixit jälginud
aasta aega ja ma olen kindel, et see muutub veel paremaks.

# Pinephone (2020)
[Pinephone](https://www.pine64.org/pinephone/) on nutitelefon mis on mõeldud jooksutama alternatiivseid linuxi
operatsioonisüsteeme. Erinevalt tavalistest androidi telefonidest ei ole
pinephone täis igasugust tarkvara mida ei saa ära installida. Selle asemel
jooksutab pinephone täiesti vabu op-süsteeme.

Pinephone hakkas päriselt inimesteni jõudma 2020 jaanuaris. Mistõttu on telefon
vägagi uus ja selle tarkvara on vägagi algelises faasis.

Ma ostsin endale Pinephone'i kuna ma ei tahtnud google'i telefoni, aga ma pole
seda tänaseni korralikult kasutada saanud kuna selle tarkvara on liiga algelises
arengufaasis.

See telefon on huvitav kuna see on odav alternatiiv kõikidele androidi
telefonidele.
